import pymysql
import pymysql.cursors
from flask import jsonify


connect = pymysql.connect(host='localhost', user='root',
                          password='', db='plant_api', port=3306)


def getplant():
    with connect.cursor(pymysql.cursors.DictCursor) as cursor:

        # define request
        sql = "SELECT * FROM flower"

        # execute
        cursor.execute(sql)

        # store
        rows = cursor.fetchall()
        result = []
        result.append(rows)
        # rows into json
        resp = jsonify(rows)

        # set the retun code
        # resp.status_code = 200

        # send response
        return result


def getCurrentPlant():
    with connect.cursor(pymysql.cursors.DictCursor) as cursor:

        # define request
        sql = "SELECT * FROM flower WHERE planteActuelle = 1"

        # execute
        cursor.execute(sql)

        # store
        rows = cursor.fetchone()

        # send response
        return rows["Photo"]


def choosePlant(id):
    with connect.cursor(pymysql.cursors.DictCursor) as cursor:
        sql = "UPDATE flower SET planteActuelle = 0 WHERE planteActuelle = 1"
        cursor.execute(sql)
        sql = "UPDATE flower SET planteActuelle = 1 WHERE id = " + id + ""
        cursor.execute(sql)
        return '<script>window.location.href = "http://localhost:5000/"</script>'


def newPlant(Nom, Categorie, Description, Photo, Humidite, Temperature, Luminosite, Periode_de_floraison):
    with connect.cursor(pymysql.cursors.DictCursor) as cursor:
        print(Nom)
        # sql = "INSERT INTO login (nom, description) VALUES ('" + Nom + "', '" + Description + "')"
        sql = "INSERT INTO login (nom, categorie, description, photo, humidite, temperature, luminosite, periode) VALUES ('" + Nom + "', '" + Categorie + \
            "','" + Description + "', '" + Photo + "','" + Humidite + "', '" + \
            Temperature + "','" + Luminosite + "', '" + Periode_de_floraison + "')"
        cursor.execute(sql)
    return "Succès, vous avez rajouté une fleur dans votre base de données. Vous pouvez aller vérifier votre base de données en allant sur localost/phpmyadmin"
