from flask import Flask, request, render_template
import bdd

app = Flask(__name__)
@app.route("/")
def home():
    plants = bdd.getplant()
    return render_template('main.html', plant=plants)


@app.route("/plantesget", methods=["GET"])
def getplant():
    return bdd.getplant()


@app.route("/choosePlant", methods=["GET"])
def choosePlant():
    return bdd.choosePlant(request.args.get('id'))


@app.route("/newPlant", methods=["POST"])
def newPlant():
    #     return bdd.newPlant(request.form["name"], request.form["description"])
    return bdd.newPlant(request.form["name"], request.form["categorie"], request.form["description"], request.form["photo"],  request.form["humidite"],  request.form["temperature"],  request.form["luminosite"], request.form["periode_de_floraison"])


@app.route("/currentPlant")
def getCurrentPlant():
    return "<img src='" + bdd.getCurrentPlant() + "'>"


if __name__ == "__main__":
    app.run()
