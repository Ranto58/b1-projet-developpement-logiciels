-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 28 juin 2019 à 07:10
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `plant_api`
--

-- --------------------------------------------------------

--
-- Structure de la table `flower`
--

DROP TABLE IF EXISTS `flower`;
CREATE TABLE IF NOT EXISTS `flower` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) NOT NULL,
  `Categorie` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `Photo` text NOT NULL,
  `Humidite` int(11) NOT NULL,
  `Temperature` int(11) NOT NULL,
  `Luminosite` varchar(255) NOT NULL,
  `Periode_de_floraison` varchar(255) NOT NULL,
  `planteActuelle` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `flower`
--

INSERT INTO `flower` (`id`, `Nom`, `Categorie`, `Description`, `Photo`, `Humidite`, `Temperature`, `Luminosite`, `Periode_de_floraison`, `planteActuelle`) VALUES
(7, 'Pommier commun', 'Malus domestica', 'Les pommiers sont des arbres du genre botanique Malus et de la famille des Rosacées, dont le fruit est la pomme.', 'http://plantinfo.co.za/wp-content/uploads/2015/11/1395754681_malus-domestica-main1.jpg', 10, 20, 'moyenne', 'Mai', 0),
(6, 'ceriser', 'prunus avium', 'Arbre pouvant atteindre 30m de haut.Écorce gris pourpré ; pelant en lanières horizontales, avec des bandes de lenticelles rugueuses.', 'https://bauchery.fr/1463-large_default/prunus-avium-merisier.jpg', 10, 23, 'moyenne', 'avril', 0),
(3, 'Sapin', 'abies alba mill', 'Le sapin est un grand arbre résineux ', 'https://www.florealpes.com/photos/abiesalba_3.jpg?22062019064802&PHPSESSID=hq011sqtm4aq9ggc12tpct5685', 15, 18, 'moyenne', 'avril', 0),
(4, 'erable', 'acer campestre', 'Arbre assez élevé, à écorce crevassée-subéreuse', 'https://www.promessedefleurs.com/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/A/c/Acer-campestre-83907-1.jpg', 8, 23, 'moyenne', 'mai', 0),
(5, 'Achillea millefolium', 'Plante vivace a racine rampante', 'tiges Axe aérien ou souterrain ', 'https://cdn3.volusion.com/wkav7.pckj3/v/vspfiles/photos/HER-ACH01-2.jpg?1524233236', 15, 20, 'forte', 'juillet', 0),
(8, 'Pissenlit', 'Astéracées, Composées', 'Le Pissenlit est une plante vivace, à souche épaisse, à racine longue, fusiforme, de la grosseur d\'un doigt, brune, rougeâtre.', 'https://f.jwwb.nl/public/v/e/p/temp-gatwjuxfstjoscwbyhqz/acutangulumFN-1.jpg', 10, 23, 'moyenne', 'mai', 0),
(9, 'vigne', 'Vitis vinifera', 'La plupart des vignes sont des plantes grimpantes des régions au climat tempéré ou de type méditerranéen. ', 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/Vineyard_in_Montone.jpg/330px-Vineyard_in_Montone.jpg', 10, 23, 'forte', 'avril', 0),
(10, 'parasol', 'Pinidae	Pinopsida', 'reconnaissable à son port caractéristique évoquant un parasol déployé', 'https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Pino_Monserrato.JPG/1024px-Pino_Monserrato.JPG', 17, 20, 'forte', 'avril', 0),
(11, 'acacia', 'Robinia pseudoacacia', 'Cet arbre présente des fleurs zygomorphes caractéristiques chez les Fabacées. ', 'https://www.mountroyalseeds.com/wp-content/uploads/2016/10/Robinia-pseudoacacia_2-640x427.jpg', 18, 20, 'moyenne', 'mai', 0),
(12, 'unedo', 'Magnoliopsida, arbousier', 'parfois appelé arbre à fraises, est une espèce d\'arbuste ou de petit arbre de la famille des Ericaceae son fruit est appelé arbouse', 'https://wir.skyrock.net/wir/v1/resize/?c=isi&im=%2F2501%2F91542501%2Fpics%2F3235351579_2_2_3XdrN3Kx.jpg&w=760', 14, 25, 'forte', 'juillet\r\n', 1);

-- --------------------------------------------------------

--
-- Structure de la table `login`
--

DROP TABLE IF EXISTS `login`;
CREATE TABLE IF NOT EXISTS `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `photo` text NOT NULL,
  `humidite` int(11) NOT NULL,
  `temperature` int(11) NOT NULL,
  `luminosite` varchar(255) NOT NULL,
  `periode` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `login`
--

INSERT INTO `login` (`id`, `nom`, `categorie`, `description`, `photo`, `humidite`, `temperature`, `luminosite`, `periode`) VALUES
(5, 'aze', 'aze', 'aze', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fstorage.googleapis.com%2Fneris%2Fpublic%2Fimages%2Ftest-header-3.svg&imgrefurl=https%3A%2F%2Fwww.16personalities.com%2Ffr%2Ftest-de-personnalite&docid=b2EKdwgohwnrNM&tbnid=LLSqc5L4G3MOTM%3A&vet=10ahUKEwjjsdTLvorjAhXM5-AKHSt-ApUQMwhSKAAwAA..i&w=800&h=800&bih=767&biw=1440&q=test&ved=0ahUKEwjjsdTLvorjAhXM5-AKHSt-ApUQMwhSKAAwAA&iact=mrc&uact=8', 1, 1, 'aze', 'aze'),
(6, 'test', 'test', 'test', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.recto-versoi.com%2Fsites%2Fdefault%2Ffiles%2Finline-images%2Ftest-d-orientation-scolaire_0.jpg&imgrefurl=https%3A%2F%2Fwww.recto-versoi.com%2Ftest-orientation-scolaire&docid=issBevgj0SqjuM&tbnid=4m4maOrkm5i0eM%3A&vet=10ahUKEwjjsdTLvorjAhXM5-AKHSt-ApUQMwhVKAMwAw..i&w=680&h=434&bih=767&biw=1440&q=test&ved=0ahUKEwjjsdTLvorjAhXM5-AKHSt-ApUQMwhVKAMwAw&iact=mrc&uact=8', 1, 1, 'zze', 'zez'),
(7, 'azd', 'azd', 'azd', 'https://www.google.com/imgres?imgurl=http%3A%2F%2Fdz-skischool.fr%2Fwp-content%2Fuploads%2F2016%2F01%2Flogo_home.png&imgrefurl=http%3A%2F%2Fdz-skischool.fr%2F&docid=e1-LEQLlZYSWLM&tbnid=6fyl2Mei0mJY5M%3A&vet=10ahUKEwjS6dfWzYrjAhVk8eAKHTSGBdMQMwhNKBUwFQ..i&w=444&h=131&bih=767&biw=1440&q=dz&ved=0ahUKEwjS6dfWzYrjAhVk8eAKHTSGBdMQMwhNKBUwFQ&iact=mrc&uact=8', 12, 1, 'zf', 'efz'),
(8, 'Plante1', 'PLANTE', 'Ceci est un test', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.sftcdn.net%2Fimages%2Ft_app-cover-l%2Cf_auto%2Fp%2Fbefbcde0-9b36-11e6-95b9-00163ed833e7%2F260663710%2Fthe-test-fun-for-friends-screenshot.jpg&imgrefurl=https%3A%2F%2Fthe-test-fun-for-friends.fr.softonic.com%2Fiphone&docid=BQMuJHr8FLJEaM&tbnid=hj6en-hBZN-VPM%3A&vet=10ahUKEwjHtsjt0YrjAhWRx4UKHS1NBvMQMwhTKAEwAQ..i&w=1020&h=574&bih=767&biw=1440&q=test&ved=0ahUKEwjHtsjt0YrjAhWRx4UKHS1NBvMQMwhTKAEwAQ&iact=mrc&uact=8', 12, 12, 'forte', 'mai'),
(9, 'test', 'test', 'etst', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fimages.sftcdn.net%2Fimages%2Ft_app-cover-l%2Cf_auto%2Fp%2Fbefbcde0-9b36-11e6-95b9-00163ed833e7%2F260663710%2Fthe-test-fun-for-friends-screenshot.jpg&imgrefurl=https%3A%2F%2Fthe-test-fun-for-friends.fr.softonic.com%2Fiphone&docid=BQMuJHr8FLJEaM&tbnid=hj6en-hBZN-VPM%3A&vet=10ahUKEwjHtsjt0YrjAhWRx4UKHS1NBvMQMwhTKAEwAQ..i&w=1020&h=574&bih=767&biw=1440&q=test&ved=0ahUKEwjHtsjt0YrjAhWRx4UKHS1NBvMQMwhTKAEwAQ&iact=mrc&uact=8', 1, 1, 'zrzer', 'zer'),
(10, 'tet', 'tet', 'etet', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.anunciantes.com%2Fwp-content%2Fuploads%2F2016%2F02%2Faea_new.jpg&imgrefurl=https%3A%2F%2Fwww.anunciantes.com%2Fsobre-la-aea%2F&docid=FGTpyNvQeVteJM&tbnid=pn6kI39lZH8YRM%3A&vet=10ahUKEwionr3004rjAhXPzIUKHZ30DTsQMwhBKAAwAA..i&w=261&h=261&bih=718&biw=1440&q=aea&ved=0ahUKEwionr3004rjAhXPzIUKHZ30DTsQMwhBKAAwAA&iact=mrc&uact=8', 12, 12, 'geg', 'zrg'),
(11, 'gege', 'gege', 'df', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fwww.anunciantes.com%2Fwp-content%2Fuploads%2F2016%2F02%2Faea_new.jpg&imgrefurl=https%3A%2F%2Fwww.anunciantes.com%2Fsobre-la-aea%2F&docid=FGTpyNvQeVteJM&tbnid=pn6kI39lZH8YRM%3A&vet=10ahUKEwionr3004rjAhXPzIUKHZ30DTsQMwhBKAAwAA..i&w=261&h=261&bih=718&biw=1440&q=aea&ved=0ahUKEwionr3004rjAhXPzIUKHZ30DTsQMwhBKAAwAA&iact=mrc&uact=8', 21, 12, 'dfb', 'zeg');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
